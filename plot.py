import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import maxwell

f = np.loadtxt('run10.dat', delimiter=',')
x = f.flatten()
rv = maxwell()
plt.plot(x, rv.pdf(x), 'k-', lw=2)

n, bins, patches = plt.hist(x, normed=1,alpha=0.7)
plt.xlabel('$V_{rel} [km/s]$')
plt.ylabel('Probability')
plt.savefig('hist.png')


