import math
import sys
import numpy as np
from amuse.lab import *
from amuse.couple import bridge
from amuse.units.optparse import OptionParser
from amuse.units import quantities

from amuse.ext.galactic_potentials import MWpotentialBovy2015
from amuse.ext.protodisk import ProtoPlanetaryDisk

from amuse.ext.composition_methods import *
import matplotlib.pyplot as plt

from prepare_figure import single_frame
from distinct_colours import get_distinct

class drift_without_gravity(object):

    def __init__(self, particles, time= 0 |units.Myr):
        self.particles=particles
        self.model_time= time
    def evolve_model(self, t_end):
        dt= t_end- self.model_time
        self.particles.position += self.particles.velocity*dt
        self.model_time= t_end
    def add_particles(self, p):
        self.particles.add_particles(p)
    @property
    def potential_energy(self):
        return quantities.zero
    @property 
    def kinetic_energy(self):
        return (0.5*self.particles.mass*self.particles.velocity.lengths()**2).sum()
    def stop(self):
        return


def integrate_particle_in_potential(cluster, disk, t_end, dt, converter, eps2):
    filename = 'cluster.hdf5'
    galaxy_model = MWpotentialBovy2015()
        
    cluster_gravity = Huayno(converter)
    cluster_gravity.particles.add_particles(cluster)
    cluster_gravity.parameters.epsilon_squared = eps2
    channel_from_cluster_to_framework = cluster_gravity.particles.new_channel_to(cluster)
    
    #disk_gravity = BHTree(converter)
    #disk_gravity.particles.add_particles(disk)
    #disk_gravity.parameters.epsilon_squared = eps2
    disk_gravity = drift_without_gravity(disk)
    channel_from_disk_to_framework = disk_gravity.particles.new_channel_to(disk)    
    
    gravity = bridge.Bridge(use_threading=False)
    gravity.add_system(cluster_gravity, (galaxy_model,) )
    gravity.add_system(disk_gravity, (galaxy_model, cluster_gravity) )
    #t_orb = 2*np.pi*sun.position.length()/sun.velocity.length()
    gravity.timestep = min(dt, 10|units.Myr)

    Etot_init = gravity.kinetic_energy + gravity.potential_energy
    Etot_prev = Etot_init

    time = 0.0 | t_end.unit
    x = []
    y = []
    near = []
    while time < t_end:
        time += dt

        gravity.evolve_model(time)
        channel_from_cluster_to_framework.copy()
        channel_from_disk_to_framework.copy()
        write_set_to_file(gravity.particles, filename, "hdf5")
        x.append(gravity.particles[0].x.value_in(units.kpc))
        y.append(gravity.particles[0].y.value_in(units.kpc))
        
        nearlist = []
        vel = cluster.nearest_neighbour(disk).velocity
        for i, v in enumerate(vel):
            relvel = (v-cluster[i].velocity).value_in(units.kms)
            nearlist.append(np.sqrt(relvel[0]**2+relvel[1]**2+relvel[2]**2))
        near.append(nearlist)

        Etot_prev_se = gravity.kinetic_energy + gravity.potential_energy

        Ekin = gravity.kinetic_energy 
        Epot = gravity.potential_energy
        Etot = Ekin + Epot

        print "T=", time, 
        print "E= ", Etot, "Q= ", Ekin/Epot,
        print "dE=", (Etot_init-Etot)/Etot, "ddE=", (Etot_prev-Etot)/Etot 

        Etot_prev = Etot
    gravity.stop()
    return x, y, near

def main(N, W0, t_end, n_steps, filename, Mtot, Rvir):
    np.random.seed(111)
    colors = get_distinct(6)
    dt = t_end/float(n_steps)
    Ndisk = 1000

    sun = Particles(1)
    sun.mass= 1 | units.MSun
    sun.position= [-7977.0, 2764.0, -106.4] | units.parsec
    sun.velocity= [88.45, 212.7, -2.788] | units.kms
    converter=nbody_system.nbody_to_si(Mtot, Rvir)
    cluster = new_king_model(N, W0=3, convert_nbody=converter)
    cluster.mass = new_salpeter_mass_distribution(len(cluster), 0.1|units.MSun, 10.0|units.MSun)
    eps2 = 10000 | units.AU**2
    cluster.scale_to_standard(convert_nbody=converter, smoothing_length_squared = eps2)
    cluster.position += sun.position
    cluster.velocity += sun.velocity
    disk = ProtoPlanetaryDisk(Ndisk, convert_nbody=converter, densitypower=1.5, Rmin=600, Rmax=1100, q_out=1.0, discfraction=1.0).result
    #galaxy_model = MWpotentialBovy2015()
    #disk.velocity += galaxy_model.circular_velocity(disk.position.length())
    disk.move_to_center()
    
    #print galaxy_model.circular_velocity(disk.position.length())
    #disk.mass = np.ones(Ndisk)*1e-10 | units.MSun
    
    fig = plt.figure(figsize=(10,10))
    x, y, near = integrate_particle_in_potential(cluster, disk, t_end, dt, converter, eps2)
    
    # Question 1 plot
    plt.scatter(cluster.x.value_in(units.kpc),cluster.y.value_in(units.kpc), c='b', alpha=0.5, s=cluster.mass.value_in(units.MSun)*100)
    plt.scatter(disk.x.value_in(units.kpc),disk.y.value_in(units.kpc), c='k', alpha=0.3, s=disk.mass.value_in(units.MSun)*100)
    plt.scatter([0], [0], marker="+", s=300, c='r')
    #plt.axis('equal')
    plt.xlabel("X [kpc]")
    plt.ylabel("Y [kpc]")
    plt.xlim(-10, 10)
    plt.ylim(-10, 10)
    plt.savefig("rama.png")
    plt.close(fig)
    
    # Question 2 plot
    fig2 = plt.figure(figsize=(12,8))
    t = np.arange(0, 4.6, 0.1)
    near_mean = [np.mean(np.array(x)) for x in near]
    near_median = [np.median(np.array(x)) for x in near]
    near_std = [np.std(np.array(x)) for x in near]
    plt.plot(t, near_mean, label='mean')
    plt.plot(t, near_median, label='median')
    plt.plot(t, near_std, label='dispersion')
    plt.legend()
    plt.xlabel('time [Gyr]')
    plt.ylabel('$V_{rel}$ [km/s]')
    plt.savefig('velocity.png')
    plt.close(fig2)

def new_option_parser():
    result = OptionParser()
    result.add_option("-n", dest="n_steps", type="float", default = 46,
                      help="number of diagnostics time steps [%default]")
    result.add_option("-f", dest="filename", default = "proto_solar_cluster.hdf5",
                      help="output filename [%default]")
    result.add_option("-N", dest="N", type="int",default = 100,
                      help="number of stars [%default]")
    result.add_option("-M", unit=units.MSun,
                      dest="Mtot", type="float",default = 100 | units.MSun,
                      help="cluster mass [%default]")
    result.add_option("-R", unit= units.parsec,
                      dest="Rvir", type="float",default = 10 | units.parsec,
                      help="cluser virial radius [%default]")
    result.add_option("-t", unit= units.Gyr,
                      dest="t_end", type="float", default = 4.6 | units.Gyr,
                      help="end time of the simulation [%default]")
    result.add_option("-W", 
                      dest="W0", type="float", default = 7.0,
                      help="Dimension-less depth of the King potential (W0) [%default]")
    return result

if __name__ in ('__main__', '__plot__'):
    o, arguments  = new_option_parser().parse_args()
    main(**o.__dict__)

