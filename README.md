# README #

### question 1 & 2
Run code 'exam.py', which generates two figures 'rama.png' for question 1, and 'velocity.png' for question 2.
The velocities of the simulated particels are much larger than the observed velocity of 'Oumuamua.

### question 3
I ran 10 runs with the shell script 'run10.sh', and generate the plot with 'plot.py'. Failed to fit with Maxwellian distribution.

### question 4
I think the age of the 'Oumuamua may be young. According to the results in 'velocity.png', the velocity dispersions tend to be smaller at the early ages.

### question 5
I may use the N-body code to replace the simple drifter, and increase the number of stars in cluster, or change the galaxy potential to a live galaxy.

* Name: Yapeng Zhang
* Student Number: 2009749

